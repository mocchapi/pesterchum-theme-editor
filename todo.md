 
# The list of bullshit i gotta add to this bitch

- [x] Searching in the main view
- [ ] (maybe) letting the user jump between positions of blocks in the main view, compact view, & json source
- [ ] right clicking items in the asset viewer to open the file itself, or the containing directory
- [x] highlighting/displaying blocks where a file is used when selected in the asset viewer
	- This can now be done by searching the filename
- [x] make assetBlock not reset the typing cursor when the process trigger goes off
- [x] minimize item tooltip has a typo
- [ ] (maybe) implement ctrl+z undo by keeping a history of modified blocks in EditorsManager
	- this wont work for deleted blocks!!! maybe keep those in a seperate tree? idunno
- [ ] more style block widget keys
- [x] image style widget
- [ ] automatically find pchum theme folder
- [ ] confirm exit if unsaved
- [x] fonts
- [ ] right click a visual block to add a child instead of having to scroll up
- [x] detect unused files
- [ ] automatically restructure theme into filtered folders
	- like, audio in "sounds", fonts in "fonts", icons in "textures" or whatever
	- textures could have sub-directories. like for the mood icons or something
- [x] assetBlock suggestions are all lowercase!! wtf!!
- [ ] download & edit themes from the theme repository