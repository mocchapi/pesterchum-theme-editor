extends MarginContainer

var tween:SceneTreeTween
export var tween_trans := Tween.TRANS_CUBIC
export var fade_time := 0.3

func _ready():
	pass

func fade_in(start:=true)->SceneTreeTween:
	if tween:
		tween.kill()
	tween = get_tree().create_tween()
	tween.set_trans(tween_trans)
	tween.tween_callback(self, "show")
	tween.tween_interval(0.05)
	tween.tween_property(self, "modulate", Color(1,1,1,1.0), fade_time)
	tween.tween_interval(0.1)
	if start:
		tween.play()
	return tween

func fade_out(start:=true)->SceneTreeTween:
	if tween:
		tween.kill()
	tween = get_tree().create_tween()
	tween.set_trans(tween_trans)
	tween.tween_callback(self, "show")
	tween.tween_interval(0.05)
	tween.tween_property(self, "modulate", Color(1,1,1,0), fade_time)
	tween.tween_interval(0.05)
	tween.tween_callback(self, "hide")
	if start:
		tween.play()
	return tween
