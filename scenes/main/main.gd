extends Control

onready var editors_manager = $"%EditorsManager"
onready var tree_directory = $"%tree_directory"
onready var file_preview = $"%file_preview"
onready var lbl_status = $"%lbl_status"
onready var overlay = $"%overlay"
onready var menu_file = $"%menu_file"


var all_files = []
const icons = {
	'audio':preload("res://assets/texture/icons/w95/audio.png"),
	'directory':preload("res://assets/texture/icons/w95/directory.png"),
	'category':preload("res://assets/texture/icons/w95/category.png"),
	'array':preload("res://assets/texture/icons/w95/directory.png"),
	'unknown':preload("res://assets/texture/icons/w95/unknown.png"),
	'plain':preload("res://assets/texture/icons/w95/unknown.png"),
	'style':preload("res://assets/texture/icons/w95/stylefile.png"),
	'image':preload("res://assets/texture/icons/w95/image.png"),
	'vector':preload("res://assets/texture/icons/w95/vector.png"),
	'font':preload("res://assets/texture/icons/w95/font.png"),
}

var modified = false
var saved = Time.get_unix_time_from_system()


const colors = {
	'audio': Color.turquoise,
	'directory': Color.wheat,
	'category': Color.wheat,
	'array': Color.wheat,
	'style': Color.greenyellow,
	'image': Color.orchid,
	'unknown': Color.white,
	'plain': Color.white,
	'color': Color.yellowgreen,
	'font': Color.chocolate,
}

func get_used_assets()->Array:
	var out := []
	for block in get_tree().get_nodes_in_group("block_class_fileref"):
		out.append(extract_block_data(block))
	return out

func set_status(new:String):
	var t = Time.get_time_dict_from_system()
	var out = '['+str(t['hour']).pad_zeros(2)+ ':' +str(t['minute']).pad_zeros(2)+':' +str(t['second']).pad_zeros(2)+'] '
	out += new
	lbl_status.text = out

func find_struct_item(path:Array, be_quiet:=false):
#	print()
#	print("finding ",path)
	var curr = Data.structure
#	print(curr.keys())
	var idx = 0
	for item in path:
#		print(idx,'>',item)
		if curr['type'] == 'category':
			if curr['children'].has(item):
				curr = curr['children'][item]
			else:
				if not be_quiet:
					printerr("couldnt find ",path.slice(0,idx),' in ',curr['children'].keys())
				return null
		elif curr['type'] == 'array':
			if typeof(item) != TYPE_INT:
				item = 0
				if not be_quiet:
					printerr("index ",item," is not an integer! defaulting to 0")
#				assert(false)
#				return null
			
			if item >= 0 and item < curr['children'].size():
				curr = curr['children'][item]
			else:
				if len(curr['children']) == 1:
					curr = curr['children'][0]
					# Hack for the moods array
				else:
					if not be_quiet:
						printerr("index ",item,' falls out of bounds in ',curr['children'].size())
					assert(false)
					return null

		else:
			if not be_quiet:
				printerr("Early termination! ",curr,' is not a category or array!')
			return null
		idx += 1
#	print("Found :) ",curr.keys())
	return curr

func extract_block_data(block):
	if block.has_method('get_data'):
		return block.get_data()
	return block.block_values

func _ready():
#w	print(structure_items)
	load_file(Data.target_file)
	overlay.fade_out()

func read_file(file:String):
	var f = File.new()
	var out = ""
	f.open(file, f.READ)
	out = f.get_as_text()
	f.close()
	return out

func save_file(file_path:String=Data.target_file):
	var json = editors_manager.process_changes()
	
	var f = File.new()
	f.open(file_path, f.WRITE)
	f.store_string(json)
	f.close()
	set_status("Saved to "+file_path)
	saved = Time.get_unix_time_from_system()
	modified = false

func _add_recent(file_path:String):
	var f := File.new()
	var out := []
	if not f.file_exists('user://recents.txt'):
		f.open('user://recents.txt', f.WRITE)
		f.store_string(file_path)
		f.close()
	else:
		f.open('user://recents.txt', f.READ)
		out = f.get_as_text().split('\n', false)
		f.close()
		var idx = out.find(file_path)
		if idx != -1:
			out.remove(idx)
		out.insert(0, file_path)
		f.open('user://recents.txt', f.WRITE)
		f.store_string('\n'.join(out))
		f.close()

func refresh_files_tree():
	tree_directory.clear()
	all_files = []
	tree_directory.load_directory(Data.target_file.substr(0, len(Data.target_file) - len(Data.target_file.split('/')[-1])))

func load_file(file_path:String):
	$"%lbl_filepath".text = file_path
	var dir = file_path.substr(0, len(file_path) - len(file_path.split('/')[-1]))
	_add_recent(file_path)
	tree_directory.load_directory(dir)
	
	
	
#	print(dir + 'style.js')
	var json = read_file(file_path)
#	print(json)
	editors_manager.load_json(json)
	set_status("Loaded file")

func spawn_query(q:Node):
	$"%queries".add_child(q)

func _on_tree_directory_file_selected(path, relpath, filename, filetype):
	file_preview.preview(path, relpath, filename, filetype)


func guess_docstring(key, value, path):
	var struct_search = find_struct_item(path + [key]) 
	if struct_search != null:
		return struct_search['doc']

	return ""

func guess_block_type(key, value, path:=[], be_quiet:=false):
	var struct_search = find_struct_item(path + [key], be_quiet)
	if struct_search != null:
		return struct_search['type']
	
	var t = typeof(value)
	key = str(key)
	
	if t == TYPE_DICTIONARY:
		return 'category'
	elif t == TYPE_ARRAY:
		if key in ["loc", 'size']:
			return 'vector'
		return 'array'
	elif key in ['style', 'selectedstyle', 'selected']:
		return 'style'
	elif key in ['background-image', 'icon', 'newmsgicon', 'image']:
		return 'image'
	elif key in ['color', 'newmsgcolor', 'systemMsgColor']:
#		print(">color ",key,' : ',value,' ~~ ', '/'.join(path))
		return 'color'
	elif 'sounds' in path:
		return 'audio'
	elif key == 'defaultmood' or (key =='mood' and t in [TYPE_INT, TYPE_REAL]):
		return 'mood'
	elif t in [TYPE_INT, TYPE_REAL]:
		return 'number'
	return "plain"



func _on_EditorsManager_processed():
	modified = true # TODO: Warn if exiting without saving
	tree_directory.check_used(get_used_assets())


func _on_EditorsManager_timeout():
	if menu_file.autosave:
		save_file()
