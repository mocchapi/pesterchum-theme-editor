extends VBoxContainer
const scn_block_container = preload("res://scenes/visualblocks/blockContainer.tscn")
const scn_array_container = preload("res://scenes/visualblocks/arrayContainer.tscn")

const scn_array_block = preload("res://scenes/visualblocks/arrayBlock.tscn")

const scn_simple_block = preload("res://scenes/visualblocks/simpleBlock.tscn")

onready var main = $"/root/main"
onready var btn_add_block = $"../../HBoxContainer/btn_add_block"

const dict_mode = true

const block_types = {
	'plain': scn_simple_block,
	'vector': preload("res://scenes/visualblocks/vectorBlock.tscn"),
	'asset': preload("res://scenes/visualblocks/assetBlock.tscn"),
	'audio': preload("res://scenes/visualblocks/audioBlock.tscn"),
	'image': preload("res://scenes/visualblocks/imageBlock.tscn"),
	'color': preload("res://scenes/visualblocks/colorBlock.tscn"),
	'mood': preload("res://scenes/visualblocks/moodBlock.tscn"),
	'number': preload("res://scenes/visualblocks/numberBlock.tscn"),
	'style': preload("res://scenes/visualblocks/styleBlock.tscn"),
	'font': preload("res://scenes/visualblocks/fontBlock.tscn"),
	
	'halignment': preload("res://scenes/visualblocks/halignBlock.tscn"),
	'valignment': preload("res://scenes/visualblocks/valignBlock.tscn"),
	
	'category': scn_array_block,
	'array': scn_array_block,
}

var key_lookup = {}

func _ready():
	set_meta('path',[])

func build_tree(dict:Dictionary):
	for child in get_children():
		remove_child(child)
		child.queue_free()
	var block_root = _recurse(dict)
	block_root.get_node("child_holder").unique_name_in_owner = true
	btn_add_block.kidd = block_root
#	print(key_lookup.values()[3])
	print(key_lookup)
#	breakpoint
	add_child(block_root)

func create_dict():
	return main.extract_block_data(get_child(0))


func make_block_instance(key, item_block_type, path:=[], value=null, from_array:=false):
	printerr("MAKING BLOCK INSTANCE!!! :: ",key,', ',item_block_type,', ',path,', ',from_array,', ')
	var new_container
	if from_array:
		new_container = scn_array_container.instance()
	else:
		new_container = scn_block_container.instance()
	var docstring = main.guess_docstring(key, value, path)
	var new_block
	if item_block_type in ['category', 'array']:
		if value != null:
			new_block = _recurse(value, path + [0 if from_array else key])
		else:
			new_block = block_types[item_block_type].instance()
		new_container.show_hide_button = true
		new_block.dict_mode = item_block_type == 'category'
	else:
		new_block = block_types[item_block_type].instance()
		if value != null:
			new_block.block_values = value
	
	printerr("NEW BLOCK!!: ",new_block)
	var _key = 0 if from_array else key
	new_container.name = key
	new_container.set_meta('key',_key)
	new_block.set_meta('path', path+[_key])
	key_lookup[Data.path2str(path+[_key])] = new_block 
	new_container.set_meta('path',(path + [_key]))
	if from_array:
		new_container.get_node("%lbl_idx").text = str(key)
	else:
		new_container.get_node("%lbl_type").text = item_block_type
		new_container.get_node("%lbl_key").text = '"' + key + '"'

	new_container.get_node('%lbl_docstring').text = docstring
	new_container.get_node("%child_holder").add_child(new_block)
	new_container.get_node("%img_icon").texture = main.icons.get(item_block_type, main.icons['unknown'])
	new_container.get_node("%name_panel").self_modulate = main.colors.get(item_block_type, main.colors['unknown'])
	new_container.get_node("%holder_panel").self_modulate = main.colors.get(item_block_type, main.colors['unknown'])

	
	return new_container


func get_keys():
	if get_child_count() > 0:
		return get_child(0).get_keys()
	return []
#	for item in get_children():
#		print(item,': ',item.get_meta('key'),'!!')
#		out.append(item.get_meta('path'))
#	return out

func _recurse(items, path:=[], x=0):
	assert(x < 1000)
#	breakpoint
	
	var t = typeof(items)
	
	assert(not t == TYPE_INT)
	# Parent object (DictBlock or ArrayBlock) 
#	var out = scn_block_container.instance() if t == TYPE_DICTIONARY else scn_array_container.instance()
	var out = scn_array_block.instance()
	var out_child_holder = out.get_node("%child_holder")
	
	var t_is_dict = t == TYPE_DICTIONARY
	out.dict_mode = t_is_dict
	out.set_meta('path',path)
	key_lookup[Data.path2str(path)] = out
	
	var idx = -1
	for item in items:
		idx += 1
		
		var new_block
		var new_container
		var item_block_type
		var docstring
		if t_is_dict:
			if items[item] == null:
				printerr("SILENTLY IGNORED ",path,' ',item, ' BECAUSE ITS NULL!')
				idx +=1
				continue
			# TODO: use make_block_instance() instead
			new_container = scn_block_container.instance()
			item_block_type = main.guess_block_type(item, items[item], path)
			docstring = main.guess_docstring(item, items[item], path)
			if item_block_type in ['category', 'array']:
				new_block = _recurse(items[item], path + [item], x+1)
				new_container.show_hide_button = true
			else:
				new_block = block_types[item_block_type].instance()
				new_block.block_values = items[item]
				new_block.set_meta('path', path+[item])
			
			new_container.name = item
			new_container.set_meta('key',item)
			new_container.set_meta('path',(path + [item]))
			new_container.get_node("%lbl_type").text = item_block_type
			new_container.get_node("%lbl_key").text = '"' + item + '"'
		else:
			if item == null:
				printerr("SILENTLY IGNORED ",path,' ',idx, ' BECAUSE ITS NULL!')
				idx +=1
				continue
			new_container = scn_array_container.instance()
			item_block_type = main.guess_block_type(idx, item, path)
			docstring = main.guess_docstring(idx, item, path)

			if item_block_type in ['category', 'array']:
				new_block = _recurse(item, path + [idx], x+1)
				new_container.show_hide_button = true
			else:
				new_block = block_types[item_block_type].instance()
				new_block.block_values = item
				new_block.set_meta("path", path + [idx])
			
			new_container.name = str(idx)
			new_container.set_meta('key',str(idx))
			new_container.set_meta('path',(path + [idx]))
			new_container.get_node("%lbl_idx").text = str(idx)
		
		new_container.get_node('%lbl_docstring').text = docstring
		new_container.get_node("%child_holder").add_child(new_block)
		new_container.get_node("%img_icon").texture = main.icons.get(item_block_type, main.icons['unknown'])
		new_container.get_node("%name_panel").self_modulate = main.colors.get(item_block_type, main.colors['unknown'])
		new_container.get_node("%holder_panel").self_modulate = main.colors.get(item_block_type, main.colors['unknown'])
		
		key_lookup[Data.path2str(new_block.get_meta('path'))] = new_block

		
		out_child_holder.add_child(new_container)
		
#		var key = item if t_is_dict else idx
#		var val = items[key] if t_is_dict else item
		# Key for dict or Value for array!!
		pass

	return out
#	var out = guess_block(key, value)



#func guess_block(key, value):
#	pass
