extends VBoxContainer

onready var img_preview = $img_preview
onready var lbl_relpath = $HBoxContainer/lbl_relpath
onready var lbl_filename = $HBoxContainer/lbl_filename
onready var lbl_type = $lbl_type


func preview(path, relpath, filename, filetype):
	var preview_img = $"/root/main".icons[filetype]
	if filetype == 'image':
		if filename.ends_with('.gif'):
			var gifreader = GifReader.new()
			preview_img = gifreader.read(path+filename)
		else:
			var tmp = Image.new()
			tmp.load(path + filename)
			preview_img = ImageTexture.new()
			preview_img.create_from_image(tmp,1)
	#		preview_img = load(path + filename)
		filetype += ' ('+str(preview_img.get_width())+'x'+str(preview_img.get_width())+')'
	elif filetype == 'style':
		preview_img = preload("res://icon.png")
	img_preview.texture = preview_img
	lbl_relpath.text = relpath
	lbl_filename.text = filename
	lbl_type.text = filetype
