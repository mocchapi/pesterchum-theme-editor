extends TextEdit

onready var main = $'/root/main'


var do_colors := false
func set_text(new:String):
	if do_colors:
		_apply_colors(new)
	else:
		do_colors = true
	var prev_v = scroll_vertical
	var prev_h = scroll_horizontal
	text = new
	scroll_vertical = prev_v
	scroll_horizontal = prev_h

func _apply_colors(new):
	print("applying JSON colors")
	# Supremely terrible code <3
	clear_colors()
	var path = []
	for line in new.split('\n'):
#		print()
#		print(path, line)
		var strip_line = line.strip_edges()
		var start = len(line) - len(line.strip_edges(true, false))
		var split_line = line.split(': ',true, 1)
#		print(split_line)
		if strip_line.ends_with(': {'):# or strip_line.ends_with(': ['):
			var key = split_line[0].strip_edges()
			key = key.substr(1,len(key)-2)
			var block_type  = (main.guess_block_type(key, null, path, true))
#			print('~',block_type)
			add_color_region(split_line[0], split_line[1], main.colors.get((block_type), main.colors['unknown']), true)
			path.append(key)

		elif strip_line.ends_with(': ['):
			var key = split_line[0].strip_edges()
			key = key.substr(1,len(key)-2)
			add_color_region(split_line[0], split_line[1], main.colors.get(main.guess_block_type(key, 0, path, true), main.colors['unknown']), true)
			path.append(0)
		elif ': ' in line:
			var key = split_line[0].strip_edges()
			key = key.substr(1,len(key)-2)
			var block_type  = (main.guess_block_type(key, null, path, true))
			if block_type == 'plain':
				continue
#			print('>~',block_type,' ',split_line[0], split_line[1])
			add_color_region(split_line[0], split_line[1], main.colors.get(block_type, main.colors['unknown']), true)
#			add_keyword_color(line,  main.colors.get(block_type, main.colors['unknown']))
		elif strip_line.ends_with('],') or line.ends_with(']'):
#			print('popped!')
			path.pop_back()
			pass
		elif strip_line.ends_with('},') or line.ends_with('}'):
#			print('popped!')
			path.pop_back()
			pass
		else:
			pass
			
	print("Json colors applied")
#	for item in Data.structure_items:
#		add_color_region()
#		pass
