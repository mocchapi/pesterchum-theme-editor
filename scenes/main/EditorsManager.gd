extends Node

export var visual_editor_path:NodePath
export var text_editor_path:NodePath
export var arrange_editor_path:NodePath
export var tree_json_path:NodePath

onready var timer = $timer

signal timeout()
signal processed()

var text_editor:TextEdit
var visual_editor:Container
var tree_json:Tree
var arrange_editor:GraphEdit

var obj:Dictionary


func _ready():
	text_editor = get_node(text_editor_path)
	visual_editor = get_node(visual_editor_path)
	tree_json = get_node(tree_json_path)
	arrange_editor = get_node(arrange_editor_path)

func load_json(inp:String):
	obj = parse_json(inp)
	text_editor.set_text(inp)
	visual_editor.build_tree(obj)
	tree_json.build_tree(obj)

func visual_changed(obj):
#	print("Updated: ",obj)
	pass
#	print(obj)
	timer.start(1)


func _on_timer_timeout():
	print('timeout')
	emit_signal("timeout")
	process_changes()

func process_changes():
	print('processing changes')
	obj = visual_editor.create_dict()
	var json = JSON.print(obj, "	")
	tree_json.clear()
	tree_json.build_tree(obj)
	text_editor.set_text(json)
	get_parent().set_status("Processed changes")
	$"%lbl_inherits_not_shown".visible = obj.get('inherits') != ""
	emit_signal("processed")
	return json
