extends LineEdit

onready var debounce_timer = $debounce_timer
onready var tree_json = $"%tree_json"


func _ready():
	pass


func _on_line_search_tree_text_changed(new_text):
	debounce_timer.start()


func _on_debounce_timer_timeout():
	tree_json.perform_search(text)
	pass # Replace with function body.
