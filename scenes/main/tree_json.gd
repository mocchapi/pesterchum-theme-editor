extends Tree

onready var main = $"/root/main"

var all_leafs := []


func perform_search(text:String):
	text = text.to_lower()
	print("Search for ",text)
	var to_select = null
	var idx = 0
	for leaf in all_leafs:
		leaf = leaf as TreeItem
		leaf.clear_custom_bg_color(0)
		if text == "":
			leaf.set_text(0, leaf.get_meta("realtext"))
			leaf.collapsed = false
		elif text in leaf.get_meta('realtext').to_lower():
			if to_select == null:
				to_select = leaf
			print("Found: ",leaf.get_meta("realtext"),' @ ',leaf.get_meta("pathinfo")[4])
			leaf.set_text(0, leaf.get_meta("realtext"))
			leaf.set_custom_bg_color(0, Color.green, true)
			leaf.collapsed = false
			var next = leaf.get_parent()
			while next != null:
				next.collapsed = false
				next.set_text(0, next.get_meta("realtext"))
				next = next.get_parent()
		else:
#			if leaf.is_selected(0):
#				leaf.deselect(0)
#			var info = leaf.get_meta("pathinfo")
			leaf.collapsed = true
			leaf.set_text(0, "")
	if to_select != null:
		_scrollto(to_select)


func _recurse(obj, p:TreeItem, path:=[]):
	var t = typeof(obj)
	var idx := -1
	for x in obj:
		idx += 1
		var key = x if t == TYPE_DICTIONARY else idx
		var val = obj[x] if t == TYPE_DICTIONARY else x
		var leaf = create_item(p)
		all_leafs.append(leaf)
		
		var block_type = main.guess_block_type(key, val, path)
		
		var valstring
		if block_type in ['array', 'category']:
			if val == null:
				continue
			_recurse(val, leaf, path + [key])
			valstring = ('{...}' if t == TYPE_DICTIONARY else '[...]')

		else:
			valstring = str(val)
		leaf.set_tooltip(0, str(key) + ': ' + valstring + '\n\n Type: ' + block_type + '\n Path: ' + '/'.join(path+[key]))
		leaf.set_text(0, str(key) + ': ' + valstring)
		leaf.set_custom_color(0, main.colors.get(block_type, main.colors['unknown']))
		leaf.set_icon(0, main.icons.get(block_type, main.icons['unknown']))
		leaf.set_meta('pathinfo', [path, key, val, block_type, Data.path2str(path+[key])])
		leaf.set_meta("realtext",  str(key) + ': ' + valstring)

func build_tree(obj:Dictionary):
	all_leafs.clear()
	var r = get_root()
	if r == null:
		r = create_item()
		r.set_meta("realtext","")
	_recurse(obj, r, [])


func _scrollto(leaf):
	scroll_to_item(leaf)
	var path = leaf.get_meta("pathinfo")[4]
	var lookup = main.editors_manager.visual_editor.key_lookup
	if lookup.has(path):
		main.editors_manager.visual_editor.get_parent().ensure_control_visible(lookup[path])

func _on_tree_json_item_activated():
	var item = get_selected()
	print("Item activated: ",item.get_meta('realtext'),' :: ',item)
	_scrollto(item)
	pass # Replace with function body.
