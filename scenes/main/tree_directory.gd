extends Tree

onready var main = $"/root/main"

signal file_selected(path, relpath, filename, filetype)

var all_leafs := []

func _recurse(dir:String, p:TreeItem, i=0, sub=0):
	var dirs = EasyDirectory.walk_directory(dir, true, true, false)
	dirs.sort()
	for item in dirs:
		var type = 'unknown'
		if item.ends_with('/'):
			type = 'directory'
			var leaf = create_item(p)
			all_leafs.append(leaf)
			leaf.set_text(0, item)
			leaf.set_icon(0,main.icons[type])
			leaf.set_custom_color(0,main.colors[type])
			leaf.set_tooltip(0, dir + item)
			main.all_files.append([dir, dir.substr(sub), item, type])
			leaf.set_meta('fileinfo', main.all_files[-1])
			_recurse(dir + item, leaf, i+1, sub)
		else:
			var leaf = create_item(p)
			all_leafs.append(leaf)
			var icon 
			var realitem = item
			item = item.to_lower()
			if item.ends_with('.js') or item.ends_with('json'):
				type = 'style'

			elif item.ends_with('.wav'): # or item.ends_with('.mp3') or item.ends_with('.ogg'):
				# TIL only wav is supported!
				type = 'audio'

			elif item.ends_with('.png') or item.ends_with('.jpeg') or item.ends_with('.png') or item.ends_with('.jpg') or item.ends_with('.gif'):  
				type = 'image'

			elif item.ends_with('.ttf') or item.ends_with('.otf'):
				type = 'font'

			leaf.set_text(0, realitem)
			leaf.set_icon(0, main.icons[type])
			leaf.set_tooltip(0, dir + realitem)
			leaf.set_custom_color(0, main.colors[type])
			main.all_files.append([dir, dir.substr(sub), realitem, type])
			leaf.set_meta('fileinfo', main.all_files[-1])
	pass

func load_directory(dir:String):
	var root = get_root()
	if not root:
		root = create_item()
	all_leafs = []
	_recurse(dir, root, 0, len(dir))
		
#	print(path)

func check_used(used:Array):
	print(main.all_files[0])
#	print(':::',used[0])
	for leaf in all_leafs:
		leaf as TreeItem
		var fileinfo = leaf.get_meta('fileinfo')
		var combipath = fileinfo[1] + fileinfo[2]
		if fileinfo[-1] == 'directory' or combipath == 'style.js':
			leaf.clear_custom_bg_color(0)
			continue
		var is_used = ('$path/' + combipath in used or  'url($path/'+combipath+')' in used)
		if is_used:
			leaf.clear_custom_bg_color(0)
#			leaf.set_custom_bg_color(0, Color.green, true)
		else:
			leaf.set_custom_bg_color(0, Color.red, true)

func get_drag_data(position): # begin drag
#	set_drop_mode_flags(DROP_MODE_INBETWEEN | DROP_MODE_ON_ITEM)
	var selected = get_selected()
	
	var data = selected.get_meta('fileinfo')
	
	var out = data[1] + data[2]
	
	var preview = Label.new()
	preview.text = out
	set_drag_preview(preview) # not necessary

	return out

func _on_tree_directory_item_activated():
	var selected = get_selected()
	if selected:
		var meta = selected.get_meta('fileinfo')
		if len(meta) == 4:
			emit_signal("file_selected", meta[0],meta[1],meta[2],meta[3])


func _on_tree_directory_item_selected():
	pass # Replace with function body.
	_on_tree_directory_item_activated()
