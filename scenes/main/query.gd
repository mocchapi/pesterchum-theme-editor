extends "res://scenes/main/overlay.gd"

onready var btn_cancel = $btn_cancel

export var show_on_ready := true
export var can_cancel := true
export var free_on_cancel := true

signal cancelled()

func _ready():
	hide()
	modulate = Color.transparent
	if show_on_ready:
		fade_in(true)
	else:
		hide()


func _on_btn_cancel_pressed():
	if can_cancel:
		cancel()

func cancel():
	emit_signal("cancelled")
	var t = fade_out(false)
	if free_on_cancel:
		t.tween_callback(self,'queue_free')
	t.play()
