extends MenuButton


onready var main := $"/root/main"

var autosave := false

func _ready():
	get_popup().connect("index_pressed", self, "_on_index_pressed")


func _on_index_pressed(idx):
	var text = get_popup().get_item_text(idx)
	if text == "Reload current project":
		var tween = $"%overlay".fade_in(false)
		tween.tween_callback(Data, "load_main_scene")
		tween.play()
	elif text == "Save":
		main.save_file()
	elif text == "Return to start screen":
		Data.load_start_scene()
	elif text == "Autosave":
		autosave = not autosave
		get_popup().set_item_checked(idx, autosave)
