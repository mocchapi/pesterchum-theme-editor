extends "res://scenes/main/query.gd"

signal ok(key, type)

func _ready():
	$"%line_keyname".grab_focus()


func ok():
	emit_signal("ok",$"%line_keyname".text, $"%option_type".get_item_text($"%option_type".selected))
	cancel()
