extends "res://scenes/main/query.gd"

var path := []
onready var main = $'/root/main'
onready var VE = $"/root/main/%visualeditor"

onready var line_keyname = $"%line_keyname"
onready var option_type = $"%option_type"

var _ignore_signal := false
var disable_guess := false

signal ok(key, block_type)

func _ready():
	line_keyname.grab_focus()
	
	_ignore_signal = true
	for key in VE.block_types:
		option_type.add_item(key)
	_ignore_signal = false


func _on_line_keyname_text_changed(new_text):
	pass # Replace with function body.
	if not disable_guess:
		_guess()


func _on_option_type_item_selected(index):
	if not _ignore_signal:
		disable_guess = true

func _guess():
	var type = main.guess_block_type(line_keyname.text, null, path)
	
	print('guessed ',type,' from ',line_keyname.text)
	
	var idx = 0
	for key in VE.block_types:
		if key == type:
			_ignore_signal = true
			option_type.select(idx)
			_ignore_signal = false
			break
		idx += 1


func _on_btn_ok_pressed():
	$"%btn_ok".disabled = true
	emit_signal("ok", line_keyname.text, option_type.get_item_text(option_type.selected))
	cancel()
