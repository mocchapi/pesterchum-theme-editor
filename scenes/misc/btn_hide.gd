extends Button


onready var child_holder = get_parent().get_parent().get_node("%child_holder")

func _ready():
	toggle_mode = true

func _toggled(button_pressed):
	child_holder.visible = not button_pressed
