extends ItemList

export var search_text:String setget search
onready var main = $"/root/main"

export(Array, String) var shown_types := []

func _ready():
	pass # Replace with function body.
	_reinit()
	search(search_text)
	connect("visibility_changed", self, '_on_vis_changed')

func _on_vis_changed():
	if visible:
		search(search_text)

func _reinit():
	clear()
	
	var real = search_text.to_lower()
	if real.begins_with('$path/'):
		real = real.substr(len('$path/'))
	
	var idx = 0
	for filedata in main.all_files:
		if real != "":
			if (not real in (filedata[1].to_lower()+filedata[2].to_lower()) ):
				idx += 1
				continue
		if (len(shown_types) > 0 and not (filedata[3] in shown_types)):
			idx += 1
			continue
		
		add_item(filedata[1] + filedata[2], main.icons[filedata[3]])
		set_item_custom_fg_color(get_item_count()-1, main.colors[filedata[3]])
		pass

func search(new:String):
	search_text = new
	_reinit()
