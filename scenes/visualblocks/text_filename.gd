extends LineEdit

func can_drop_data(position, data):
	return typeof(data) == TYPE_STRING

func drop_data(position, data):
	pass
	yield(get_tree(),"idle_frame")
	get_parent().set_block_values(data)
#	set_text(data)
