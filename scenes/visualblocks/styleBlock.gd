extends VBoxContainer

onready var btn_add = $"%btn_add"
#onready var btn_del = $"%btn_del"
onready var styles_holder = $styles_holder

onready var main = $'/root/main'

var block_values setget set_block_values

const scn_entry_query = preload("res://scenes/misc/query/style_entry_query.tscn")

const scn_widget_container = preload("res://scenes/visualblocks/widgets/widgetContainer.tscn")
const scn_simple_block = preload("res://scenes/visualblocks/simpleBlock.tscn")

const _widget_color = preload("res://scenes/visualblocks/colorBlock.tscn")
const _widget_plain = preload("res://scenes/visualblocks/simpleBlock.tscn")
const _widget_image = preload("res://scenes/visualblocks/widgets/assetWidget.tscn")
const widget_names = {
	'color': _widget_color,
	'plain': _widget_plain,
	'image': _widget_image,
}
const style_widgets = {
	'color':_widget_color,
	'background-color': _widget_color,
	'background-image': _widget_image,
	'selection-background-color':_widget_color,
	
	'font-size':_widget_plain,
	'font-family':_widget_plain,
	'font-weight':_widget_plain, # Make PX spinbox?
	'font-style':_widget_plain, # Make dropdown
	
	'border-style':_widget_plain, # Make dropdown
	'border-width':_widget_plain, # Make PX spinbox?
	'border-color':_widget_color,

#	'border-color':_widget_color,
	
}

func _reinit():
	for item in styles_holder.get_children():
		styles_holder.remove_child(item)
		item.queue_free()
	
	var used_names = []
	btn_add.get_popup().clear()
	
	for item in block_values.split(';'):
		item = item.strip_edges()
		if item == '':
			continue
		var w_name = item.split(':')[0]
		var w_data = item.substr(len(w_name)+1).strip_edges()
		var widget = construct_widget(w_name, w_data)
		styles_holder.add_child(contain(widget))
		used_names.append(w_name)

	
	for i in style_widgets:
		if not i in used_names:
			btn_add.get_popup().add_item(i)

func construct_widget(w_name, data):
	var widget = style_widgets.get(w_name, scn_simple_block).instance()
	widget.block_values = data
	widget.name = w_name
	widget.set_meta('key', w_name)
	return widget

func contain(item):
	var out = scn_widget_container.instance()
	out.name = item.name
	out.set_meta('key',item.name)
	out.get_node("%lbl_key").text = item.name
	out.get_node("%child_holder").add_child(item)
	return out

func get_str():
	var out = []
	for item in styles_holder.get_children():
		out.append(str(block_values))
	return '; '.join(out)

func _ready():
	btn_add.get_popup().connect("index_pressed", self, '_on_btn_add_pressed')
	set_block_values(block_values)


func set_block_values(new_values):
	printerr("SET TO ",new_values)
	if new_values == null:
		new_values = ''
		breakpoint
	block_values = new_values
	if is_inside_tree():
		_reinit()
		$"/root/main/EditorsManager".visual_changed(self)

func prompt():
	var inst = scn_entry_query.instance()
	inst.connect("ok", self, '_on_ok')
	$'/root/main'.spawn_query(inst)

func _on_ok(key, type):
	var new = widget_names.get(type,widget_names['plain']).instance()
	new.name = key
	new.set_meta('key', key)
	new.block_values = null
	styles_holder.add_child(contain(new))

func _on_btn_add_pressed(idx):
#	print("HII ",idx)
	var add_name = btn_add.get_popup().get_item_text(idx)
	if btn_add.get_popup().get_item_id(idx) == 0:
		prompt()
		pass # Custom property, prompt for name
	else:
		styles_holder.add_child(contain(construct_widget(add_name,null)))


func form_block_values():
	var out = ''
	for widget in styles_holder.get_children():
		var val =main.extract_block_data(widget)
#		if typeof(val) == TYPE_COLOR:
#			val = val.to_html(true)
		out += widget.get_meta('key') +': '+ str(val) +'; '
	return out

func update_add_menu():
	var popup = btn_add.get_popup()
	popup.allow_search = true
	popup.clear()
	var not_added_yet = style_widgets.keys()
	for child in styles_holder.get_children():
		var key = child.get_meta('key')
		if key in not_added_yet:
			not_added_yet.erase(key)
	
	popup.add_item('Custom property', 0)
	popup.add_separator()
	
	not_added_yet.sort()
	var idx = popup.get_item_count()
	for w_name in not_added_yet:
		popup.add_item(w_name)

func get_data():
	return form_block_values()


func _on_btn_add_about_to_show():
	update_add_menu()
