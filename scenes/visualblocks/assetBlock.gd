extends VBoxContainer

onready var text_filename = $text_filename
onready var list_files = $list_files
onready var img_preview = $img_preview

export var data_prefix:String=""
export var data_suffix:String=""

onready var main = $"/root/main"

func _on_text_filename_focus_entered():
	list_files.show()
	img_preview.hide()


func _on_text_filename_focus_exited():
	list_files.hide()
	img_preview.show()


var block_values setget set_block_values

func _ready():
	add_to_group("block_class_fileref")
	set_block_values(block_values)

func set_block_values(new_values):
	if new_values == null:
		new_values = ''
	block_values = format_text(new_values)
	if text_filename:
		text_filename.text = block_values
		list_files.search(deformat_text(block_values))

	if $'/root/main/EditorsManager':
		$timer.stop()
		_update_preview()
		$"/root/main/EditorsManager".visual_changed(self)

func _on_text_filename_text_changed(new_text):
	list_files.search(deformat_text(new_text))
	$timer.start(0.5)

func deformat_text(txt:String)->String:
	var out = txt
	if out.begins_with(data_prefix):
		out = out.substr(len(data_prefix))
	if out.ends_with(data_suffix):
		out = out.substr(0, len(out) - len(data_suffix))
	if out.begins_with('$path/'):
		out = out.substr(len("$path/"))
	return out

func get_preview():
	var searchname = deformat_text(block_values)
	
	for file in main.all_files:
		if (file[1] + file[2]) == searchname:
			var img
			if file[3] == 'image':
				var tmp = Image.new()
				if file[2].ends_with('.gif'):
					var reader = GifReader.new()
					img = reader.read(file[0]+file[2])
				elif tmp.load(file[0] + file[2]) == OK:
					img = ImageTexture.new()
					img.create_from_image(tmp,1)
				else:
					img = null
			else:
				img = main.icons.get(file[3], main.icons['unknown'])
			return img
	return null

func _update_preview():

	img_preview.texture = get_preview()

func _on_list_files_item_selected(index):
	var path = list_files.get_item_text(index)
	_on_text_filename_focus_exited()
	set_block_values(path)

func format_text(txt:String)->String:
	var out = txt.strip_edges()
	if not out.begins_with(data_prefix+'$path/'):
		if out.begins_with('$path/'):
			out = data_prefix + out
		elif out.begins_with(data_prefix):
			out = data_prefix + '$path/' + out.substr(len(data_prefix))
		else:
			out = data_prefix + '$path/' + out
	
	if not out.ends_with(data_suffix):
		out += data_suffix

	return out

func _on_timer_timeout():
	print("!!!timeout")
	var new_values = format_text(text_filename.text)

	block_values = new_values

	list_files.search(deformat_text(block_values))

	$timer.stop()
	_update_preview()
	$"/root/main/EditorsManager".visual_changed(self)




func _on_text_filename_gui_input(event):
	if event.is_action("ui_cancel"):
		text_filename.release_focus()
