extends SpinBox

var block_values setget set_block_values
var ignore_signal = false

func _ready():
	set_block_values(block_values)

func set_block_values(new_values):
	if not typeof(new_values) in [TYPE_REAL, TYPE_INT]:
		new_values = 0
	ignore_signal = true
	block_values = new_values
	value = block_values
	if $'/root/main/EditorsManager':
		$"/root/main/EditorsManager".visual_changed(self)
	ignore_signal = false

func _on_numberBlock_value_changed(value):
	if not ignore_signal:
		set_block_values(value)
	pass # Replace with function body.
