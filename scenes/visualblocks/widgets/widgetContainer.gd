extends HBoxContainer

func get_data():
	return $"/root/main".extract_block_data($"%child_holder".get_child(0))


func _ready():
	add_to_group("block_class_searchable")

func _on_btn_delete_pressed():
	$"/root/main/EditorsManager".visual_changed(self)
	queue_free()
