extends OptionButton



var block_values setget set_block_values

func _ready():
	set_block_values(block_values)

var ignore_signal := false

func set_block_values(new_values):
	if typeof(new_values) != TYPE_STRING:
		ignore_signal = true
		select(0)
		ignore_signal = false
		block_values = get_item_text(selected)
	else:
		var found = false
		for i in get_item_count():
			if get_item_text(i) == new_values:
				block_values = new_values
				ignore_signal = true
				select(i)
				ignore_signal = false
				found = true
				break
		if not found:
			ignore_signal = true
			select(0)
			ignore_signal = false
			block_values = get_item_text(selected)

	if $'/root/main/EditorsManager':
		$"/root/main/EditorsManager".visual_changed(self)

func get_data():
	return get_item_text(selected)


func _on_halignBlock_item_selected(index):
	set_block_values(get_item_text(index))
	pass # Replace with function body.
