extends GridContainer

onready var spin_x = $spin_x
onready var spin_y = $spin_y

var ignore_signal = false
var block_values setget set_block_values

func _ready():
	set_block_values(block_values)

func get_data():
	return block_values

func set_block_values(new_values):
	if typeof(new_values) != TYPE_ARRAY:
		new_values = [0,0]
	ignore_signal = true
#	print("Set vector:",new_values)
	if len(new_values) == 0:
#		print("SET TO [0,0]")
		new_values = [0,0]
	elif len(new_values) == 1:
#		print("APPEND")
		new_values.append(0)
#	else:
#		print("CHECKS CLEARED")
#	print("bv:",block_values)
	block_values = new_values
#	print('~~',block_values)
#	print(typeof(block_values[0]),' ',typeof(block_values[1]),' ')

	if spin_x and spin_y:
		spin_x.value = block_values[0]
		spin_y.value = block_values[1]
#		print('x:',spin_x.value, ' y:', spin_y.value)
		if $'/root/main/EditorsManager':
			$"/root/main/EditorsManager".visual_changed(self)
#	else:
#		print("<><> not in tree yet")
#	print("bv:",block_values)
#	print()
	ignore_signal = false

func _on_spin_value_changed(value):
	if ignore_signal:
		return
#	print("signal!")
	user_set_value(spin_x.value, spin_y.value)


func user_set_value(x,y):
	block_values = [round(x),round(y)]
	$"/root/main/EditorsManager".visual_changed(self)
	
