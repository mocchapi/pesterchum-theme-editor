extends MenuButton

const scn_block_query = preload("res://scenes/misc/query/block_query.tscn")

export var kidd_name = NodePath('%child_holder')

onready var kidd = get_node(kidd_name).get_child(0)
onready var VE = $"/root/main/%visualeditor"
onready var main = $'/root/main'

func _ready():
	connect("about_to_show", self, "_on_about_to_show")
	get_popup().connect("index_pressed", self, "_on_index_pressed")


func _on_about_to_show():
	var pop = get_popup()
	pop.clear()
	
	pop.add_item('Custom item',0)
	pop.add_separator()
	var addable_structs = find_addable_structs()
	for struct in addable_structs:
		var out = ""
		out += struct['name'] + ' (' + struct['type']+ ')' 
		if struct['doc'] != '':
			out += (': '+struct['doc'].substr(0,20) + '...' if (len(struct['doc'])+len(out))>40 else '')
#		out += struct['name']
#		out += ' '.repeat(max(0,10-len(out)))
#		out += ' (' + struct['type'] + ')'
#		pop.add_item(struct['name'] +)
		pop.add_item(out)
		pop.set_item_tooltip(pop.get_item_count()-1, struct['name'] +'\n\n Type: '+struct['type']+'\n Purpose: '+(struct['doc'] if struct['doc'] != '' else 'not documented yet'))
		pop.set_item_metadata(pop.get_item_count()-1, struct)
		pop.set_item_icon(pop.get_item_count()-1, main.icons.get(struct['type'], main.icons['unknown']))

func find_addable_structs():
	var mypath = kidd.get_meta('path')
	print()
	print('find_addable_structs ',mypath)
	var struct = main.find_struct_item(mypath)
	if struct != null:
		var possible_keys
		if struct['type'] == 'category':
			possible_keys = struct['children'].values()
		elif struct['type'] == 'array':
			possible_keys = struct['children']
		var current_keys = get_current_keys()
		print('current keys: ',current_keys)
		var out = []
		print('possible keys: ')
		for i in possible_keys:
			print('~~ ',i['name'])
			if not i['name'] in current_keys:
				print("A-OK")
				out.append(i)
		return out
	else:
		return []

func get_current_keys():
	if kidd.has_method('get_keys'):
		var out = []
		for i in kidd.get_keys():
			out.append(i[-1])
		return out
	else:
		return []

func _on_index_pressed(idx):
	print('hi ',idx)
	if idx == 0:
		_prompt_custom()
	else:
		var struct = get_popup().get_item_metadata(idx)
		_on_ok(struct['name'], struct['type'])

func _prompt_custom():
	var inst = scn_block_query.instance()
	inst.connect("ok", self, '_on_ok')
	$'/root/main'.spawn_query(inst)

func _on_ok(key, block_type):
#	print()
	print("BT: ",block_type)
	var inst = VE.make_block_instance(key, block_type, kidd.get_meta('path'), null, not kidd.dict_mode)
	kidd.get_node('%child_holder').add_child(inst)
	kidd.get_node('%child_holder').move_child(inst,0)
