extends VBoxContainer

onready var lbl_idx = $"%lbl_key"
onready var child_holder = $"%child_holder"

var show_hide_button := false

func _ready():
	$"%btn_hide".visible = show_hide_button
	$"%btn_options".visible = false#show_hide_button
	$"%btn_add".visible = show_hide_button # TODO: make block_query work with array indexes. indices? indexices.
	$"%spacer".visible = show_hide_button
	$"/root/main/EditorsManager".visual_changed(self)

func get_data():
	return $'/root/main'.extract_block_data(child_holder.get_child(0))


func _on_btn_delete_pressed():
	$"/root/main/EditorsManager".visual_changed(self)
	queue_free()
