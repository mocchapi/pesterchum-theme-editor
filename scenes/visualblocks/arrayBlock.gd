extends VBoxContainer

onready var child_holder = $child_holder

var dict_mode := false

func get_data():
	var main = $"/root/main"
	var out
	if dict_mode:
		out = {}
	else:
		printerr("GET DATA ON ARRAY: ",get_path())
		out = []
	
	for child in child_holder.get_children():
		var data = main.extract_block_data(child)
		if dict_mode:
			out[child.get_meta('key')] = data
		else:
#			print("child ",child)
			out.append(data)
#			print(data)
	return out


func get_keys():
	var out = []
	for item in child_holder.get_children():
		out.append(item.get_meta('path'))
	return out
