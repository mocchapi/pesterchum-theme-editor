extends LineEdit


var block_values setget set_block_values

func _ready():
	set_block_values(block_values)

func set_block_values(new_values):
	block_values = new_values
	text = str(block_values)
	if is_inside_tree():
		$"/root/main/EditorsManager".visual_changed(self)

func _on_simpleblock_text_changed(new_text):
	block_values = new_text
	$"/root/main/EditorsManager".visual_changed(self)
	
func get_data():
	return text
