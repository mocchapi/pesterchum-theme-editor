extends OptionButton

var block_values setget set_block_values

func _ready():
	set_block_values(block_values)

func set_block_values(new_values):
	if new_values == null:
		new_values = 0
	block_values = new_values
	selected = new_values
	if $'/root/main/EditorsManager':
		$"/root/main/EditorsManager".visual_changed(self)


func _on_moodBlock_item_selected(index):
	block_values = selected
	$"/root/main/EditorsManager".visual_changed(self)
