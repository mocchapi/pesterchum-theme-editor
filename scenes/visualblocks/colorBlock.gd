extends ColorPickerButton



var block_values = Color.black setget set_block_values

func _ready():
	set_block_values(block_values)

func percentage_to_float(percentage:String):
	percentage = percentage.strip_edges()
	return clamp(100.0/float(percentage.substr(0, len(percentage)-1)), 0.0, 1.0)

func rgba_to_float(rgba_in:String):
	rgba_in = rgba_in.strip_edges()
	var values = []
	var is_rgb = false
	if rgba_in.begins_with('rgba('):
		values = rgba_in.substr(5,len(rgba_in)-6).split(',')
	elif rgba_in.begins_with('rgb('):
		is_rgb = true
		values = rgba_in.substr(4,len(rgba_in)-5).split(',')

	for idx in len(values)-1:
		if '%' in values[idx]:
			values[idx] = percentage_to_float(values[idx])
	
	if is_rgb:
		return Color(values[0], values[1], values[2])
	else:
		return Color(values[0], values[1], values[2], values[3])

func set_block_values(new_values):
	if typeof(new_values) == TYPE_COLOR:
		block_values = new_values
	elif typeof(new_values) == TYPE_STRING:
		if 'rgb' in new_values:
			print("RGB(A) detected: ",new_values)
#			breakpoint
			block_values = rgba_to_float(new_values)
			print("Set from RGB(A) ",block_values)
		else:
			block_values = ColorN(new_values, 1)
			if block_values == Color.black and new_values != 'black':
				block_values = Color(new_values)
		print("set color ",new_values,' -> ',block_values)
	color = block_values
	if is_inside_tree():
		$"/root/main/EditorsManager".visual_changed(self)

func get_data():
	return '#'+color.to_html(true)

func _on_colorBlock_color_changed(color):
	block_values = color
	$"/root/main/EditorsManager".visual_changed(self)
	pass # Replace with function body.
