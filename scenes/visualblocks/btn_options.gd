extends MenuButton

onready var popup := get_popup()
var menu_remove_item

func _ready():
	menu_remove_item = popup.add_submenu_item("Remove item", "removeitem")
