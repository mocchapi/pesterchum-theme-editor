extends Control

export var speed := 10.5
export var planet_rot := 50.0

func _ready():
	rect_pivot_offset.y = OS.get_window_size().y
	rect_pivot_offset.x = 0
	rect_rotation = deg2rad(rand_range(0, 360))

func _process(delta):
	rect_rotation = fmod((rect_rotation), 360)
	rect_pivot_offset.y = OS.get_window_size().y
	rect_pivot_offset.x = 0
	rect_rotation += speed*delta
	get_child(0).rect_rotation += planet_rot*delta
