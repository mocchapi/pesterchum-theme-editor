extends TextureRect

export var noise:OpenSimplexNoise
export var resolution:Vector2
export var speed := 10.0
export var randomize_seed := true

var next = true
var pos:=0.0
var pixels_per_frame := (128*8)

const max_pixels_per_frame := 128*12

var startoff = 0.0

func _ready():
	if randomize_seed:
		noise.seed = Time.get_unix_time_from_system()
		startoff = float(noise.seed)
	texture = ImageTexture.new()

func _process(delta):
	pos += delta 
	

	if next:
		texture.create_from_image(yield(_make_image(), 'completed'), 0)

func _make_image():
	next = false
	var img = Image.new()
	img.create(resolution.x, resolution.y, false, Image.FORMAT_RGBA8)
	img.lock()
	var idx = 0
	for x in range(resolution.x):
		for y in range(resolution.y):
			img.set_pixel(x,y, _get_color(x,y))
			idx += 1
			if idx % pixels_per_frame == 0:
				yield(get_tree(),"idle_frame")
#				$Label.text = str(pixels_per_frame) + ': ' + str(2-(float(pixels_per_frame)/float(max_pixels_per_frame)))
				if pos > 20:
					if Engine.get_frames_per_second() < 58:
						pixels_per_frame = max(pixels_per_frame-64, 128*4)
					else:
						pixels_per_frame = min(pixels_per_frame+32, max_pixels_per_frame)
	img.unlock()
	next = true
	return img

const border = 0.1
func _get_color(x,y):
	var alpha = 1.0
	
#	var fpos = fmod((pos+startoff)* (speed*(2-(float(pixels_per_frame)/float(max_pixels_per_frame)))), 64.0*16)
	var fpos = (pos)*speed
#	$Label2.text = str(fpos)
	
	var n_x = x
	var n_y = y
	
	var vvv = 1-((Vector2(x,y).distance_to(resolution/2))/(resolution.length()/2.5))
	
	var n_val = noise.get_noise_3d(n_x,n_y,+vvv*8)
	
	
	n_val += noise.get_noise_3d(n_x*vvv,n_y*vvv,fpos+vvv*8)
	
	
	if x % 4 == 0 or y % 4 == 0:
		n_val -=0.2
		n_val *= (sin(fpos/10) + sin(fpos/20))/2
#	n_val = n_val/2
	
#	n_val += pos/200
	
#	n_val = clamp(n_val * vvv, 1, 0)
#	n_val += vvv*vvv
#	n_val = min(n_val, (vvv+0.2)+sin(pos)*0.2)
#	n_val *= vvv+0.2

	n_val = stepify(n_val, 0.2)
	n_val += 0.1 if ((x+y) % 4) == 0 else 0
	n_val += rand_range(-0.01, 0.01)
#	n_val = max(n_val, 0)

#	return Color.from_hsv(1.0, 1.0, vvv, 1.0)
	
	return Color.from_hsv(n_val+fpos/100, 1, n_val, 1.0)
#	return Color.from_hsv(n_val, 1, 0.25+0.25*((sin(vvv)+1)/2), 1.0)
#	return Color.from_hsv(n_val+ pos/100, 1.0, n_val, 1.0)
	
	return Color.from_hsv(n_val+ pos/100, 1.0, n_val*vvv+0.2, 1.0)
	
	return Color(n_val, n_val, n_val)
