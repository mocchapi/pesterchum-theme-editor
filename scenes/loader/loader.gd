extends Control


onready var file_load = $"%file_load"
onready var line_path = $"%line_path"
onready var btn_load = $"%btn_load"

onready var line_path_create = $"%line_path_create"
onready var text_create = $"%text_create"
onready var file_create = $"%file_create"
onready var btn_create = $"%btn_create"


func _ready():
	_do_recents()
	line_path.grab_focus()
	OS.get_cmdline_args()
	_do_button_verify()

func _do_recents():
	var f := File.new()
	var out = []
	if not f.file_exists('user://recents.txt'):
		print("Doesnt exist")
		f.open('user://recents.txt', f.WRITE)
		f.store_string('')
		f.close()
	else:
		print("Exists")
		f.open('user://recents.txt', f.READ)
		out = f.get_as_text().split('\n',false)
		print(f.get_as_text())
		f.close()
		print(out)
	var list_recents = $"%list_recents"

	for i in out:
		if f.file_exists(i):
			list_recents.add_item(i)

func _verify_file(file_path:String) ->bool:
	return File.new().file_exists(file_path)
	pass

func _do_button_verify():
	btn_load.disabled = not _verify_file(line_path.text)

func _on_file_load_file_selected(path):
	line_path.text = path
	_do_button_verify()
	pass # Replace with function body.


func _on_btn_open_dia_pressed():
	file_load.popup_centered()
	_do_button_verify()
	pass # Replace with function body.


func _on_btn_load_pressed():
	Data.target_file = line_path.text
	var t = $overlay.fade_in(false)
	t.tween_callback(self, "_start_load")
	t.play()

func _start_load():
	Data.load_main_scene()

func _on_line_path_text_changed(new_text):
	pass # Replace with function body.
	_do_button_verify()




func _on_LinkButton_pressed():
	OS.shell_open('https://gitlab.com/mocchapi/pesterchum-theme-editor')


func _on_file_create_file_selected(path):
	line_path_create.text = path
	_do_button_verify_create()


func _do_button_verify_create():
	btn_create.disabled = false
	pass
#	btn_create.disabled = not _verify_file_create(line_path_create.text)

func _on_btn_create_dia_pressed():
	file_create.current_file = 'style.js'
	file_create.popup_centered()
	_do_button_verify_create()


func _on_line_path_create_text_changed(new_text):
	_do_button_verify_create()


func _on_btn_create_pressed():
	var f = File.new()
	var err = f.open(line_path_create.text, f.WRITE)
	btn_create.disabled = err != OK
	if err != OK:
		return
	f.store_string(text_create.text)
	f.close()
	Data.target_file = line_path_create.text
	var t = $overlay.fade_in(false)
	t.tween_callback(self, "_start_load")
	t.play()


func _on_list_recents_item_selected(idx):
	var text = $"%list_recents".get_item_text(idx)
	line_path.text = text
	_do_button_verify()
