extends "res://scenes/arrange editor/thingo.gd"


export var key_menu_style:String
export var key_menuitem_style:String
export var key_name_client:String
export var key_name_profile:String
export var key_name_help:String

onready var lbl_client = $stack/HBoxContainer/lbl_client
onready var lbl_profile = $stack/HBoxContainer/lbl_profile
onready var lbl_help = $stack/HBoxContainer/lbl_help
onready var glorious_hbox = $stack/HBoxContainer

var the_font = preload("res://scenes/arrange editor/menu_font.tres")

func _ready():
	pass

func strip_px(input)->int:
	if typeof(input) != TYPE_STRING:
		return 0
	return int(input.replace("px",'').strip_edges())

func update_visuals():
	.update_visuals()

	var lookup = main.editors_manager.visual_editor.key_lookup
	
	if is_real_key(key_menuitem_style):
		var style_node = lookup[key_menuitem_style]
		var margin_right_px = strip_px(extract_from_style(style_node, ['margin-right']))
		var margin_left_px = strip_px(extract_from_style(style_node, ['margin-left']))

		glorious_hbox.add_constant_override("separation", margin_right_px + margin_left_px)
	
	var text_color
	if is_real_key(key_menu_style):
		var style_node = lookup[key_menu_style]
		text_color = extract_color(style_node, ['color'])
		var font_size = extract_from_style(style_node, ['font-size'])
		if font_size == null:
			font_size = 16
		else:
			font_size = strip_px(font_size)
		printerr("Font size set to ",font_size)
		the_font.size = font_size
	
	if text_color == null:
		text_color = Color.white
	
	for pair in [[key_name_client, lbl_client, "Client"], [key_name_help, lbl_help, "Help"], [key_name_profile, lbl_profile, "Profile"]]:
		var key = pair[0]
		var lbl = pair[1]
		var default = pair[2]
		if is_real_key(key):
			lbl.text = lookup[key].block_values
		else:
			lbl.text = default
		lbl.modulate = text_color
	
