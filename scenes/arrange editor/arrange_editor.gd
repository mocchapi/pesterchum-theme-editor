extends GraphEdit

onready var main = $"/root/main"

const pck_thingo := preload("res://scenes/arrange editor/arrange_node_thingo.tscn")

func _ready():
	pass


func _on_EditorsManager_processed():
	walk_watchdog()
	check_mood_buttons()
	read_rects()

func read_rects():
	for kid in get_children():
		if kid is GraphNode:
			kid.read_rect()

func _form_mood_button(index:int):
	var base = '"main"/"moods"/'+str(index)
	var instance = pck_thingo.instance()
	instance.key_watchdog = base
	
	base += '/'
	instance.key_position = base + '"loc"'
	instance.key_size = 	base + '"size"'
	instance.key_fg_image = base + '"icon"'
	instance.key_bg_image = base + '"style"'
	instance.key_bg_color = base + '"style"'
	instance.key_label_text = base + '"text"'
	
	instance.name = 'mood_button_'+str(index)
	instance.watchdog_free_instead_of_hide = true
	return instance

func walk_watchdog():
	for kid in get_children():
		if kid is GraphNode:
			if kid.verify_watchdog(true):
				kid.update_visuals()

func check_mood_buttons():
	var obj = main.editors_manager.obj
	
	var moods:Array = obj.get('main',{}).get('moods',[])
	if moods.empty():
		return

	for idx in len(moods):
		if not has_node("mood_button_"+str(idx)):
			print("Made mood_button_",idx)
			add_child(_form_mood_button(idx))




