extends GraphNode

onready var img_fgimage = $"%img_fgimage"
onready var img_bgimage = $"%img_bgimage"
onready var clr_bgcolor = $"%clr_bgcolor"
onready var lbl_label_text = $"%lbl_label_text"

onready var lbl_coords = $"%lbl_coords"
onready var lbl_title = $"%lbl_title"

# block, pester, addchum
export var lock_in_place:bool = false
export var keep_back:bool = false

export var realtitle:String

export var key_position:String   # vectorBlock
export var key_size:String       # vectorBlock
export var key_fg_image:String   # assetBlock (imageBlock)
export var key_bg_image:String   # assetBlock (imageBlock)
export var key_bg_color:String   # 
export var key_label_text:String # 

export var key_watchdog:String # any block
# if it is an invalid instance, this node will hide itself
export var watchdog_free_instead_of_hide:bool = false
# if this is enabled, itll free itself instead

onready var main = $'/root/main'

var _lockto:Vector2

var max_img_size:=Vector2.ZERO
var isready = false

func _ready():
	isready= true
	connect("resize_request", self, '_on_resize_requested')
	connect("dragged", self, '_on_dragged')
	connect("visibility_changed", self, '_on_vis_changed')
	get_parent().connect("node_selected", self, "_on_some_node_selected")
	if not key_size:
		resizable = false
	if not realtitle:
		realtitle = name.capitalize()
	lbl_title.text = realtitle
	lbl_title.modulate.a = 0.5
	if rect_min_size == Vector2.ZERO:
		rect_min_size = Vector2.ONE
	_lockto = offset

func is_real_key(key:String):
	if key == "":
		return false
	elif main.editors_manager.visual_editor.key_lookup.has(key):
		return true
	else:
#		printerr("Key not found: ",key)
#		breakpoint
		return false

func extract_text(node):
	if node.has_method("get_data"):
		return str(node.get_data())
	elif node.get("block_values"):
		return str(node.block_values)
	return ""

func extract_image(node, key_override:Array=["background-image"]):

	if node.has_method("get_preview"):
		return node.get_preview()
	else:
		var val = extract_from_style(node, key_override)
		if val != null:
			return val
	return null

func extract_vector(node):
	var vals = node.block_values
	return Vector2(vals[0], vals[1])

func extract_from_style(node:Node, keys:Array):
	if node.get("styles_holder") != null:
		for item in node.styles_holder.get_children():
			if item.name in keys:
				return item.get_node("%child_holder").get_child(0).get_data()

func extract_color(node, key_override:Array=['background-color', 'color']):
	if typeof(node.get("block_values")) == TYPE_COLOR:
		return node.block_values
	else:
		var val = extract_from_style(node, key_override)
		if val != null:
			return val
	printerr("Failed to extract color from ",node)
	return Color.transparent

func _fit_max_img_size(image:Texture):
	if image:
		var size = image.get_size()
		max_img_size.x = max(size.x, max_img_size.x)
		max_img_size.y = max(size.y, max_img_size.y)

func update_visuals():
	max_img_size=Vector2.ZERO
	var lookup = main.editors_manager.visual_editor.key_lookup
	
	if is_real_key(key_bg_image):
		var img = extract_image(lookup[key_bg_image])
		img_bgimage.texture = img
		_fit_max_img_size(img)
	else:
		img_bgimage.texture = null
	
	if is_real_key(key_fg_image):
		var img = extract_image(lookup[key_fg_image])
		img_fgimage.texture = img
		_fit_max_img_size(img)
	else:
		img_fgimage.texture = null
	
	if is_real_key(key_bg_color):
		clr_bgcolor.color = extract_color(lookup[key_bg_color])
	else:
		clr_bgcolor.color = Color.transparent

	if is_real_key(key_label_text):
		lbl_label_text.text = extract_text(lookup[key_label_text])
	else:
		lbl_label_text.text = ""


func read_rect():
	var lookup = main.editors_manager.visual_editor.key_lookup
	
	if is_real_key(key_size):
		rect_size = extract_vector(lookup[key_size])
	else:
		rect_size = max_img_size

	if is_real_key(key_position):
		offset = extract_vector(lookup[key_position])
	else:
		offset = Vector2.ZERO


func verify_watchdog(perform_action:bool=false):
	if key_watchdog == "":
		return true
	var lookup = main.editors_manager.visual_editor.key_lookup
	if is_real_key(key_watchdog):
		var instance = lookup[key_watchdog]
		if is_instance_valid(instance):
			return true
	if perform_action:
		if watchdog_free_instead_of_hide:
			printerr(self," failed watchdog, queue_free() now")
			get_parent().remove_child(self)
			queue_free()
		else:
			printerr(self," failed watchdog, hiding!")
			hide()
	return false


func write_rect():
	printerr("WRITE RECT!")
	var lookup = main.editors_manager.visual_editor.key_lookup
	if is_real_key(key_position):
		print(">> POSITION")
		lookup[key_position].user_set_value(offset.x, offset.y)
	if is_real_key(key_size):
		print(">> SIZE")
		lookup[key_size].user_set_value(rect_size.x, rect_size.y)


func _on_vis_changed():
	if visible:
		update_visuals()
#		print('watchdog: ',key_watchdog,' = ',main.editors_manager.visual_editor.key_lookup.get(key_watchdog))

func _process(delta):
	if lbl_coords.visible:
		lbl_coords.text = ' ( '+str(round(offset.x))+', '+str(round(offset.y))+' )\n '+str(round(rect_size.x))+' x '+str(round(rect_size.y))
	if (not keep_back and not lock_in_place) or not visible:
		return
	if keep_back and not get_index() == 0:
		get_parent().move_child(self, 0)
	if lock_in_place and offset != _lockto:
		offset = _lockto

func _on_some_node_selected(node:GraphNode):
	if node == self:
		lbl_title.modulate.a = 1.0
		lbl_coords.show()
	else:
		lbl_title.modulate.a = 0.5
		lbl_coords.hide()

func _on_resize_requested(new_size:Vector2):
	print(rect_size,'->',new_size.round())
	rect_size = new_size.round()
	write_rect()

func _on_dragged(from:Vector2,to:Vector2):
	if lock_in_place:
		offset = from.round()
		_lockto = offset
	else:
		if offset != to.round():
			offset = to.round()
		write_rect()
