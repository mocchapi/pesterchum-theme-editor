# ![icon](icon.png) Pesterchum Theme Editor 2000
![main menu](doc/main_menu.png)  
For editing [pesterchum](https://github.com/Dpeta/pesterchum-alt-servers) themes  
<sup>(Note: this software does not work for [pesterchum-godot](https://gitlab.com/mocchapi/pesterchum-godot) themes)</sup>

## Features

- Block-based GUI representation of the style json for easy editing
- Visual drag & resize editor for placeable items, such as mood buttons
- Distinct blocks with previews for all types, like images & vectors
- Structure hinting lets you easily add valid blocks
- Syntax type highlighting for the JSON preview
- Fast search for finding any object or value used
- Assets panel for viewing & drag-dropping files to blocks
- Detection of unused assets
- Autosaving for quick iterating  

For upcoming features, check out the [todo file](todo.md)

![editor](doc/editor.png)  
The *main editor* view, where you'll find, edit, or remove all your existing values, as well as add new ones
![align editor](doc/editor-2.png)  
The *align editor* can be used to get an impression of what your theme will look like, and rearrange & scale various placeable objects such as mood buttons