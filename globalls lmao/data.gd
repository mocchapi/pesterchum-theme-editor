extends Node

#var target_file:String = "/home/anne/Documents/Pesterchum/pesterchum-themes/sources/trollian2.5dark/style.js"
var target_file:String = "/home/anne/Documents/Pesterchum/pesterchum-themes/sources/pibby/style.js"

func load_main_scene():
	print("Loading main scene with target file ",target_file)
	get_tree().change_scene("res://scenes/main/main.tscn")

func load_start_scene():
	get_tree().change_scene("res://scenes/loader/loader.tscn")
	

func _ready():
	for font in [preload("res://assets/font/sources/Mainframe-Regular.ttf"), preload("res://assets/font/sources/TYPOSTUCK.ttf")]:
		font.antialiased = false

var structure_items = [
	
]


func path2str(path):
	var out = []
	for item in path:
		var t = typeof(item)
		if t == TYPE_STRING:
			out.append('"'+item+'"')
		else:
			out.append(item)
	return '/'.join(out)


func mkStruct(item_name, item_type:String, item_doc:String="", item_children:Array=[]):
	var out = {
		'name':item_name,
		'type':item_type,
		'doc':item_doc,
		'children':null,
		'path': [],
	}
	if item_type in ['array', 'category']:
		out['children'] = {} if item_type == 'category' else []
		for item in item_children:
			item.path.insert(0, item_name)
			if item_type == 'array':
				out['children'].append(item)
			else:
				out['children'][item['name']] = item
	structure_items.append(out)
	return out


var structure = mkStruct(
	'root','category','root of all evil >:)', [
		mkStruct('inherits','plain','copies properties from the given theme into this one. this theme must also be installed'),
		mkStruct('metadata','category','extra information about this theme', [
			mkStruct('author','plain','your name!'),
			mkStruct('source','plain','link to the source code of this theme'),
			mkStruct('license','plain','the license this falls under. think "CC-BY-SA-4.0" or "MIT"'),
		]),
		mkStruct('main', 'category', 'the main window',[
			mkStruct('background-image','image','Basis of your theme'),
			mkStruct('style','style',''),
			mkStruct('size','vector','size of the window, best to match with background-image resolution'),
			mkStruct('icon','image','icon on taskbar and tray shown normally'),
			mkStruct('newmsgicon','image','icon shown on the tray when you have a new message'),
			mkStruct('windowtitle','plain',''),
			mkStruct('sounds','category','notification sounds', [
				mkStruct('alertsound','audio','played when a message is received'),
				mkStruct('ceasesound','audio','played when when people cease pestering'),
				mkStruct('memosound','audio','played when a memo message is received'),
				mkStruct('honk','audio','played when anyone types "honk"'),
				mkStruct('namealarm','audio','played when anyone types your name'),
			]),
			mkStruct('fonts','array','custom font files to load up',[
				mkStruct('font file', 'font', '')
			]),
			mkStruct('close','category','"x" close button', [
				mkStruct('image','image','the icon that represents the "x" close button'),
				mkStruct('loc','vector','position of the button'),
			]),
			mkStruct('minimize','category','"_" minimize button', [
				mkStruct('image','image','the icon that represents the "_" minimze button'),
				mkStruct('loc','vector','position of the button'),
			]),
			mkStruct('menubar','category','',[
				mkStruct('style','style','style of the whole top menubar'),
			]),
			mkStruct('menu','category','', [
				mkStruct('menuitem','style','styles of the individual menu buttons, useful for spacing'),
				mkStruct('style','style','styles of dropdown menus'),
				mkStruct('selected','style','style for when you hover over a dropdown item'),
				mkStruct('loc','vector','position of the menubar'),
				mkStruct('disabled','style','styles of disabled dropdown menu items'),
			]),
			mkStruct('menus','category','replaces the names for the menu buttons & the options inside', [
				mkStruct('client','category','names for the "cient" menu', [
					mkStruct('_name','plain','name of the "client" menu itself'),
					
					mkStruct('options','plain',''),
					mkStruct('memos','plain',''),
					mkStruct('userlist','plain',''),
					mkStruct('import','plain',''),
					mkStruct('reconnect','plain',''),
					mkStruct('idle','plain',''),
					mkStruct('logviewer','plain',''),
					mkStruct('randen','plain',''),
					mkStruct('console','plain',''),
					mkStruct('talk','plain',''),
					mkStruct('addgroup','plain',''),
					mkStruct('exit','plain',''),
				]),
				mkStruct('profile','category','names for the "profile" menu', [
					mkStruct('_name','plain','name of the "profile" menu itself'),
					
					mkStruct('switch','plain',''),
					mkStruct('color','plain',''),
					mkStruct('theme','plain',''),
					mkStruct('block','plain',''),
					mkStruct('quirks','plain',''),
				]),
				mkStruct('help','category','names for the "help" menu', [
					mkStruct('_name','plain','name of the "help" menu itself'),
					
					mkStruct('help','plain',''),
					mkStruct('about','plain',''),
					mkStruct('calsprite','plain',''),
					mkStruct('rules','plain',''),
					mkStruct('reportbug','plain',''),
					mkStruct('chanserv','plain',''),
					mkStruct('nickserv','plain',''),
				]),
				mkStruct('rclickchumlist','category','names for various right click menus', [
					mkStruct('pester','plain',''),
					mkStruct('removechum','plain',''),
					mkStruct('blockchum','plain',''),
					mkStruct('addchum','plain',''),
					mkStruct('unblockchum','plain',''),
					mkStruct('banuser','plain',''),
					mkStruct('voiceuser','plain',''),
					mkStruct('opuser','plain',''),
					mkStruct('quirksoff','plain',''),
					mkStruct('quirks','plain',''),
					mkStruct('report','plain',''),
					mkStruct('viewlog','plain',''),
					mkStruct('notes','plain',''),
					mkStruct('removegroup','plain',''),
					mkStruct('renamegroup','plain',''),
					mkStruct('movechum','plain',''),
					mkStruct('quirkkill','plain',''),
					mkStruct('ooc','plain',''),
					mkStruct('invitechum','plain',''),
					mkStruct('memosetting','plain',''),
					mkStruct('memonoquirk','plain',''),
					mkStruct('memohidden','plain',''),
					mkStruct('memoinvite','plain',''),
					mkStruct('memomute','plain',''),
				]),
			]),
			mkStruct('chums', 'category', 'properties of the chumroll', [
				mkStruct('style','style','style of the chumroll'),
				mkStruct('loc','vector','position of the chumroll'),
				mkStruct('size','vector','size of the chumroll'),
				mkStruct('userlistcolor','color','default name color'),
				mkStruct('moods','category','images & name colors when a user has a specific mood', [
					mkStruct('chummy','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('rancorous','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('offline','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('pleasant','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('distraught','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('pranky','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('smooth','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('ecstatic','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('relaxed','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('discontent','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('devious','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('sleek','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('detestful','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('mirthful','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('manipulative','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('vigorous','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('perky','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('acceptant','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('protective','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('mystified','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('amazed','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('insolent','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('bemused','category','',[
						mkStruct('icon','image','mood icon'),
						mkStruct('color','color','user name color')
					]),
					mkStruct('blocked','category','',[
						mkStruct('icon','image','blocked icon'),
						mkStruct('color','color','user name color')
					]),
				]),
			]),
			mkStruct('trollslum','category','properties of the trollslum window', [
				mkStruct('style','style',''),
				mkStruct('size','vector','size of the window'),
				mkStruct('label','category','properties of the text above the list',[
					mkStruct('style','style',''),
					mkStruct('text','plain',''),
				]),
				mkStruct('chumroll','category','',[
					mkStruct('style','style','style of the actual list of usernames'),
				]),
			]),
			mkStruct('mychumhandle','category','properties for the chumhandle edit box',[
				mkStruct('label','category','',[
					mkStruct('text','plain','the actual text of the label'),
					mkStruct('loc','vector','position of the label'),
#					mkStruct('size','vector',''),
					mkStruct('style','style',''),
				]),
				mkStruct('handle','category',"the current user's handle",[
					mkStruct('loc','vector',''),
					mkStruct('size','vector',''),
					mkStruct('style','style',''),
				]),
				mkStruct('colorswatch','category','properties of the color picker',[
					mkStruct('text','plain','the text displayed inside the colorswatch'),
					mkStruct('loc','vector','position of the box'),
					mkStruct('style','style',''),
					mkStruct('size','vector',''),
				]),
				mkStruct('currentMood','vector',"position of the current user's mood icon"),
			]),
			mkStruct('defaultwindow','category','default properties of all windows (quirks, profile changer, etc)', [
				mkStruct('style','style',''),
			]),
			mkStruct('addchum','category','properties for the add chum button',[
				mkStruct('style','style','style of the button normally'),
				mkStruct('pressed','style','style of the button when pressed'),
				mkStruct('loc','vector','position of the button'),
				mkStruct('size','vector','size of the button'),
				mkStruct('text','plain','text of the button'),
			]),
			mkStruct('pester','category','properties for the pester button',[
				mkStruct('style','style','style of the button normally'),
				mkStruct('pressed','style','style of the button when pressed'),
				mkStruct('loc','vector','position of the button'),
				mkStruct('size','vector','size of the button'),
				mkStruct('text','plain','text of the button'),
			]),
			mkStruct('block','category','properties for the block button',[
				mkStruct('style','style','style of the button normally'),
				mkStruct('pressed','style','style of the button when pressed'),
				mkStruct('loc','vector','position of the button'),
				mkStruct('size','vector','size of the button'),
				mkStruct('text','plain','text of the button'),
			]),
			mkStruct('defaultmood','mood','default mood of this theme'),
			mkStruct('moodlabel','category','text label indicating mood buttons',[
					mkStruct('text','plain',''),
					mkStruct('loc','vector','position of the label'),
					mkStruct('style','style',''),
			]),
			mkStruct('moods','array','mood buttons, there is no limit of the amount of buttons',[
				mkStruct('mood button','category','mood button',[
					mkStruct('style','style','style of the button'),
					mkStruct('selected','style','style of the button when selected'),
					mkStruct('loc','vector','position of the mood button'),
					mkStruct('size','vector','size of the mood button'),
					mkStruct('text','plain','the displayed text'),
					mkStruct('icon','image','the button image'),
					mkStruct('mood','mood','the mood this button sets'),
				]),
			]),
		]), # End of main
		
		mkStruct('convo','category','properties for the conversations (pesters) window',[
			mkStruct('style','style','style of the window'),
			mkStruct('scrollbar','category','properties about the scrollbar. if added, all properties must be specified',[
				mkStruct('style','style','general style of the scrollbar'),
				mkStruct('handle','style','style for the dragable part of the scrollbar'),
				mkStruct('downarrow','style','style for the down arrow button'),
				mkStruct('darrowstyle','style','style for setting the image of the down arrow button (with image:url($path/icon.png))'),
				mkStruct('uparrow','style','style for the up arrow button'),
				mkStruct('uarrowstyle','style','style for setting the image of the up arrow button (with image:url($path/icon.png))'),
			]),
			mkStruct('margins','category','',[
				mkStruct('top','number',''),
				mkStruct('left','number',''),
				mkStruct('right','number',''),
				mkStruct('bottom','number',''),
			]),
			mkStruct('size','vector','initial size of the window'),
			mkStruct('chumlabel','category','the label of who you are chatting with',[
				mkStruct('style','style',''),
				mkStruct('align','category','label alignment',[
					mkStruct('h','halignment','horizontal alignment'),
					mkStruct('v','valignment','vertical alignment'),
				]),
				mkStruct('minheight','number','the minimum height the label may be'),
				mkStruct('maxheight','number','the maximum height the label may be'),
				mkStruct('text','plain',"text of the label. use $handle as a stand-in for the user's handle"),
			]),
			mkStruct('textarea','category','this contains the pesterlog text',[
				mkStruct('style','style',''),
			]),
			mkStruct('input','category','this is the text input bar',[
				mkStruct('style','style',''),
			]),
			mkStruct('tabwindow','category','this holds the tabs?',[
				mkStruct('style','style',''),
			]),
			mkStruct('tabs','category','this holds the tabs i guess?',[
				mkStruct('style','style',''),
				mkStruct('selectedstyle','style',''),
				mkStruct('newmsgcolor','color','color of the text when there are unread messages'),
				mkStruct('tabstyle','number','probably keep this at 0'),
			]),
			mkStruct('text','category','system message overrides, like "x BEGAN PESTERING y"',[
				mkStruct('beganpester','plain','replaces "BEGAN PESTERING"'),
				mkStruct('ceasepester','plain','replaces "CEASED PESTERING"'),
				mkStruct('blocked','plain','replaces "BLOCKED"'),
				mkStruct('unblocked','plain','replaces "UNBLOCKED"'),
				mkStruct('blockedmsg','plain','replaces "DID NOT RECEIVE MESSAGE FROM"'),
				mkStruct('openmemo','plain','replaces "OPENED MEMO ON BOARD"'),
				mkStruct('joinmemo','plain','replaces "RESPONDED TO MEMO"'),
				mkStruct('closememo','plain','replaces "CEASED RESPONDING TO MEMO"'),
				mkStruct('kickedmemo','plain','replaces "YOU HAVE BEEN BANNED FROM THIS MEMO!"'),
				mkStruct('idle','plain','replaces "IS NOW AN IDLE CHUM!"'),
			]),
			mkStruct('systemMsgColor','color','the color of system messages'),
		]), # end of convo
		mkStruct('memos','category','properties for the memos window',[
			mkStruct('style','style','style of the window'),
			mkStruct('memoicon','image','icon used for memos'),
			mkStruct('scrollbar','category','properties about the scrollbar. if added, all properties must be specified',[
				mkStruct('style','style','general style of the scrollbar'),
				mkStruct('handle','style','style for the dragable part of the scrollbar'),
				mkStruct('downarrow','style','style for the down arrow button'),
				mkStruct('darrowstyle','style','style for setting the image of the down arrow button (with image:url($path/icon.png))'),
				mkStruct('uparrow','style','style for the up arrow button'),
				mkStruct('uarrowstyle','style','style for setting the image of the up arrow button (with image:url($path/icon.png))'),
			]),
			mkStruct('margins','category','',[
				mkStruct('top','number',''),
				mkStruct('left','number',''),
				mkStruct('right','number',''),
				mkStruct('bottom','number',''),
			]),
			mkStruct('size','vector','initial size of the window'),
			mkStruct('textarea','category','this contains the pesterlog text',[
				mkStruct('style','style',''),
			]),
			mkStruct('input','category','this is the text input bar',[
				mkStruct('style','style',''),
			]),
			mkStruct('tabwindow','category','this holds the tabs?',[
				mkStruct('style','style',''),
			]),
			mkStruct('label','category','the label of who you are chatting with',[
				mkStruct('style','style',''),
				mkStruct('align','category','label alignment',[
					mkStruct('h','halignment','horizontal alignment'),
					mkStruct('v','valignment','vertical alignment'),
				]),
				mkStruct('minheight','number','the minimum height the label may be'),
				mkStruct('maxheight','number','the maximum height the label may be'),
				mkStruct('text','plain',"text of the label. use $handle as a stand-in for the user's handle"),
			]),
			mkStruct('tabs','category','this holds the tabs i guess?',[
				mkStruct('style','style',''),
				mkStruct('selectedstyle','style',''),
				mkStruct('newmsgcolor','color','color of the text when there are unread messages'),
				mkStruct('tabstyle','number','probably keep this at 0'),
			]),
			mkStruct('userlist','category','properties for the list of memo participants',[
				mkStruct('width','number',''),
				mkStruct('style','style',''),
			]),
			mkStruct('time','category','properties for the memo time & slider area',[
				mkStruct('text','category','',[
					mkStruct('width','number',''),
					mkStruct('style','style',''),
				]),
				mkStruct('slider','category','',[
					mkStruct('style','style',''),
					mkStruct('groove','style',''),
					mkStruct('handle','style',''),
				]),
				mkStruct('buttons','category','',[
					mkStruct('style','style',''),
				]),
				mkStruct('arrows','category','properties for the time arrow buttons',[
					mkStruct('left','image','icon for the left arrow button'),
					mkStruct('right','image','icon for the right arrow button'),
					mkStruct('style','style',''),
				]),
			]),
			mkStruct('systemMsgColor','color','the color of system messages'),
			mkStruct('op','category','',[
				mkStruct('icon','image',''),
			]),
			mkStruct('halfop','category','',[
				mkStruct('icon','image',''),
			]),
			mkStruct('voice','category','',[
				mkStruct('icon','image',''),
			]),
			mkStruct('founder','category','',[
				mkStruct('icon','image',''),
			]),
			mkStruct('admin','category','',[
				mkStruct('icon','image',''),
			]),
		]), # end of memos
		mkStruct('toasts','category','properties for the notification popup',[
			mkStruct('width','number',''),
			mkStruct('height','number',''),
			mkStruct('style','style',''),
			mkStruct('title','category','',[
				mkStruct('style','style',''),
				mkStruct('minimumheight','number',''),

			]),
			mkStruct('content','category','',[
				mkStruct('style','style',''),
			]),
			mkStruct('icon','category','',[
				mkStruct('signout','image',''),
				mkStruct('signin','image',''),
				mkStruct('style','style',''),
			]),
		]), # end of toasts
	# End of root
	])
