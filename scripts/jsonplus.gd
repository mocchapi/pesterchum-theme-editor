extends Reference

# Parses a json string into a dictionary,
# but in a way where it can be deduced what key is where in the original string,
# as well as patching the original string with new data one without messing up the formatting too much

class jsonplusResult:
	extends Reference
	var json:String
	var dict:Dictionary

	var plus:Dictionary # Holds the lookups

func parse(json_data:String):
	
	var out = jsonplusResult.new()
	out.json = json_data
	
	var brackeys = 0
	var in_string = false
	
	var in_value = false
	var value_type = "" # string or number or dict or array
	
	var x = 0 # Char in line
	var y = 0 # Line number

	var buffer:String
	var value_stack = []

	for idx in len(json_data):
		var chr = json_data[idx]
